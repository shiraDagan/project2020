// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
 firebaseConfig :{
  apiKey: "AIzaSyBYbVDvkbg3RXVVg6FaHalkuyLNEbZrUkU",
  authDomain: "movie2020-494f0.firebaseapp.com",
  databaseURL: "https://movie2020-494f0.firebaseio.com",
  projectId: "movie2020-494f0",
  storageBucket: "movie2020-494f0.appspot.com",
  messagingSenderId: "155489859770",
  appId: "1:155489859770:web:7513e0912c3ca1a004ea83",
  measurementId: "G-THYYNWZ1ND"
 }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
