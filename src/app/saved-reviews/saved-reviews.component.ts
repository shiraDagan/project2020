import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-saved-reviews',
  templateUrl: './saved-reviews.component.html',
  styleUrls: ['./saved-reviews.component.css']
})
export class SavedReviewsComponent implements OnInit {
  reviews$ = [];
  isLoading= false;
  comments: any = [];
  userId: string;

  constructor(private service: ClassifyService, public auth: AuthService) { }

  ngOnInit() {
    //כאן נקרא לפונקציה שבסרוויס
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.service.getreviews(user.uid).subscribe(res => {
          var MovieNames = "";
          res.forEach(review => {

           
            
            if (!MovieNames.includes(review.movie))
            {
              this.reviews$.push(review);
              MovieNames+=review.movie;

            }
             
            
          });
          this.isLoading = true;
        })
      });



    this.service.getMyComments().subscribe(comments => {

      this.comments = comments;
    })
  }
  //  deleteClassify(id:string){
  //   this.service.deleteClassify(id);
  // lior itzhak is the heart, you are my ass
  // }


}
