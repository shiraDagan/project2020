import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classified-articles',
  templateUrl: './classified-articles.component.html',
  styleUrls: ['./classified-articles.component.css']
})
export class ClassifiedArticlesComponent implements OnInit {
  userId:string;
  imageJson:Object={"0":"assets/dislike-icon.png","1":"assets/like-icon.png","2":"assets/na-icon.png"};
  categories:object[] = [{id:0,cat: 'dislike'}, {id:1,cat: 'like'},{id:2,cat: 'unknown'}];
  category:string = "Loading...";
  categoryImage:string; 
  didLike:string;
 
  
  
  onSelect(value){
    this.categoryImage = this.imageJson[value];
    this.category = this.categories[parseInt(value)]['cat'];
}

  constructor(public classifyService:ClassifyService, private authService:AuthService, private routers:Router, public activatedRoute:ActivatedRoute) { }

  ngOnInit() {

   

     this.classifyService.classify().subscribe(
      res => {
        if(res==0)
        {
        this.category = this.categories[0]['cat'];
        this.categoryImage = this.imageJson['0'];
        }
        else if(res==1){
        this.category = this.categories[1]['cat'];
        this.categoryImage = this.imageJson['1']
        }
        else {
          this.category = this.categories[2]['cat'];
          this.categoryImage = this.imageJson['2']
          }
        
        
      }
    )


    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
     })
    
  }
  addClassifyToFirestore(){
    this.classifyService.addToFirestore(this.classifyService.movieName, this.userId, 
    this.classifyService.doc,this.categoryImage); 
    this.classifyService.addCommentToFirestore(this.classifyService.movieName, this.userId,
    this.classifyService.doc,this.categoryImage); 
    this.routers.navigate(['/saved_reviews']);

         }
        
         
         
      
               
}
