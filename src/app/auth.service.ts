import { Injectable } from '@angular/core';
 //signup
import { AngularFireAuth } from '@angular/fire/auth';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { User } from './interfaces/user';

@Injectable({
 providedIn: 'root'
})
export class AuthService {
  //signup
  user: Observable<User| null>
  //login
  private logInErrorSubject = new Subject<string>();


  constructor(public afAuth:AngularFireAuth,private router: Router,private route:ActivatedRoute){ 
    this.user = this.afAuth.authState; //signup
  }

//login
  public getLoginErrors():Subject<string>{
   return this.logInErrorSubject;
  }

//signup  
signup(email:string, password:string){
  this.afAuth
  .auth.createUserWithEmailAndPassword(email,password).then
   (res => { console.log('Succesful Signup', res);
    this.router.navigate(['/welcome']);
   }
    )
  .catch(function(error) {
   // Handle Errors here.
   // var errorCode = error.code;
  var errorCode = error.code;
   var errorMessage = error.message;
  alert(errorMessage);
   console.log(error);
   console.log(errorCode);
   this.router.navigate(['/login']);
 }); 
}

//login
login(email:string, password:string){

  this.afAuth
      .auth.signInWithEmailAndPassword(email,password)
     .then(
        res => console.log('Succesful Login',res)
     ).catch (
        error => window.alert(error)
         )
        
}
//logout
Logout(){
  this.afAuth.auth.signOut();  
  this.router.navigate(['/login']);
}

//הפונקציה הזאת תעזור לנו לשלוף את המשתמש איפה שנקרא לה
getUser(){
  return this.user 
}




}