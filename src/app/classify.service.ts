import { Review } from './interfaces/review';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  private itemsCollection: AngularFirestoreCollection<Review>;
  Comments$: Comment[];
  public doc:string;
  public movieName:string;
  public movieId:Number;

  private itemsCollections: AngularFirestoreCollection<Comment>;

  private apiurl = "https://t5jjabvfk5.execute-api.us-east-1.amazonaws.com/beta";
  private url =  "https://sco4ypgnl5.execute-api.us-east-1.amazonaws.com/beta";

  
  constructor(private http: HttpClient, private db: AngularFirestore) { 

    this.itemsCollections = db.collection<Comment>('comments');
    this.itemsCollection = db.collection<Review>('classify');//ex6
  }

  getComment(){
    return this.http.get(this.apiurl)//הפלט של מה שנקבל מפונקציית גט יהיה מסוג פוסט
    }
  
  getMyComments():Observable<any[]>{
    return this.db.collection('comments').valueChanges();
  }  

//בניית פונקציית הסיווג
classify():Observable<any>{
  console.log(this.doc);
  let json = {
    "movies": [
      {
        "review": this.doc
      },
    ]
  }
  let body  = JSON.stringify(json);
  return this.http.post<any>(this.url,body).pipe(
    map(res => {
      console.log(res);
      console.log(res.body);
      let final = res.body.replace('[','');
      final = final.replace(']','');
      console.log(final);
      return final;      
    })
  );      
}

addToFirestore(_movie:string, _userId:string, _review:string, _didLike:string){
  const review = {movie:_movie, userId: _userId, review:_review, didLike:_didLike};
  this.db.collection('classify').add(review);
}

addCommentToFirestore(_movie:string,_userId:string, _review:string, _didLike:string){
  const comment = {movie:_movie,userId: _userId ,review:_review, didLike:_didLike};
  this.db.collection('comments').add(comment);
}

//הגדרת הפונקציה שתשלוף לדף זה את הגוף והסיווג
getreviews(userId):Observable<any[]>{
const collection = this.db.collection<Review>('classify', classi=>  

classi.where('userId', '==', userId))

const classify$ = collection.valueChanges();
return classify$;
}


deleteClassify(id:string){
  this.db.doc(`comments/${id}`).delete();
}


}