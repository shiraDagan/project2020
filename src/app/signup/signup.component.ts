import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  //signup
  userId:string;
  email:string;
  password:string;
  constructor(public Authservice:AuthService,private router: Router,private route:ActivatedRoute) { }

  
 
  onSubmit(){
   
    this.Authservice.signup(this.email,this.password);
      
      this.router.navigate(['/home']);
  }

  ngOnInit() {

   
  }

}
